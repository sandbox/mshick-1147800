<?php

// $Id: omniture_import.admin.inc

/**
 * @file
 * Omniture Import admin functions.
 */

function omniture_import_settings_form() {
  $form['web_services'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Web Services'),
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE
  );
  $form['web_services']['omniture_import_username'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Username'),
    '#size'	      => 50,
    '#default_value'  => variable_get('omniture_import_username', ''),
    '#description'    => t('The Web Services username from Admin > Company > Web Services.'),    
  );
  $form['web_services']['omniture_import_shared_secret'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Shared Secret'),
    '#size'	      => 50,
    '#default_value'  => variable_get('omniture_import_shared_secret', ''),
  );
  $form['web_services']['omniture_import_report_suite_id'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Report Suite ID'),
    '#size'	      => 50,
    '#default_value'  => variable_get('omniture_import_report_suite_id', ''),
  );
  $form['web_services']['omniture_import_report_epoch'] = array(
    '#type'           => 'date',
    '#title'          => t('Report Epoch'),
    '#default_value'  => variable_get('omniture_import_report_epoch', array(  'year' => 2009, 
                                                              'month' => 1, 
                                                              'day' => 1
                                                            )),
    '#description'    => t('Date Format should be YYYY-mm-dd. e.g., 2010-01-01'),
  );

  $form['cron'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Cron'),
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE
  );
      
  $form['cron']['omniture_import_hourly_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Hourly cron'),
    '#default_value' => variable_get('omniture_import_hourly_cron', 0),
    '#description' => t(''),
  );
  $form['cron']['omniture_import_hourly_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#default_value' => variable_get('omniture_import_hourly_limit', 100),
    '#description' => t('Maximum number of items to fetch in one report.'),
    '#size' => 10,
  );
  $form['cron']['omniture_import_daily_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Daily cron'),
    '#default_value' => variable_get('omniture_import_daily_cron', 0),
    '#description' => t(''),
  );
  $form['cron']['omniture_import_daily_window'] = array(
    '#type' => 'textfield',
    '#title' => t('Daily cron window (hour)'),
    '#default_value' => variable_get('omniture_import_daily_window', 1),
    '#description' => t(''),
    '#size' => 10,
  );

  $form['cron']['omniture_import_daily_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#default_value' => variable_get('omniture_import_daily_limit', 100),
    '#description' => t('Maximum number of items to fetch in one report.'),
    '#size' => 10,
  );

  $form['content'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Content'),
    '#collapsible'  => TRUE,
    '#collapsed'    => TRUE
  );
  
  $form['content']['omniture_import_time_threshold'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Page Views Threshold'),
    '#description' => t('Number of days to report for page views and referrers.'),
    '#size'	      => 10,
    '#default_value'  => variable_get('omniture_import_time_threshold', 7),
  );
  
  $node_types = node_get_types('names');
  $form['content']['omniture_import_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#default_value' => variable_get('omniture_import_node_types', array()),
    '#options' => $node_types,
    '#description' => t('Check each node type that is included in the report.'),
  );

  // Allow setting a vocabulary into a variable
  if (module_exists('taxonomy')) {
    $vocabularies = taxonomy_get_vocabularies();
    $vocabs = array();
    foreach ($vocabularies as $vid => $vocab) {
      $vocabs[$vid] = $vocab->name;
    }
    $form['content']['omniture_import_vocabularies'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Vocabularies'),
      '#default_value' => variable_get('omniture_import_vocabularies', array()),
      '#description' => t('Check each vocabulary that is included in the report. Be careful with free-tagged vocabularies, they could greatly increase the report size.'),
      '#options' => $vocabs,               
    );
  }

  $roles = user_roles(TRUE);
  $form['content']['omniture_import_user_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User Roles'),
    '#default_value' => variable_get('omniture_import_user_roles', array()),
    '#description' => t('User roles to include in the report.'),
    '#options' => $roles,
  );  

  
  return system_settings_form($form);
}